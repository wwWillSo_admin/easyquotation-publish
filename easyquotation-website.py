#!/usr/bin/env python
# encoding: utf-8

"""
@author: WillSo
@license: Apache Licence 
@software: PyCharm
@file: easyquotation-website.py
@time: 2017\12\18 0018 16:07
"""

import easyquotation
from flask import Flask, request as flaskReq, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from urllib import request,parse
import re, os, traceback, time
import json
import redis
import configparser
import pymysql
from datetime import  datetime, timedelta

# 读取配置
config=configparser.ConfigParser()
config.read('config.ini')

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

# 获取redis_client
def getRedisClient() :
    host = config.get("redis", 'ip')
    port = config.get("redis", 'port')
    password = config.get("redis", 'password')
    db = config.get("redis", 'db')
    redis_client = redis.Redis(host=host, port=port, password=password, db=db)
    return redis_client

@app.route('/getAllMarketdata',methods=['POST'])
def getAllMarketdata():
    # 接收前端发来的数据,转化为Json格式,我个人理解就是Python里面的字典格式
    data = json.loads(flaskReq.get_data())
    pageNo = data['pageNo']
    pageSize = data['pageSize']
    keyword = data['keyword']

    redis_client = getRedisClient()
    stockCodes = bytes.decode(redis_client.get('stockCodes'), encoding='utf-8')
    dataMap = redis_client.hgetall('marketdata')

    return jsonify(str(dataMap))

if __name__ == '__main__':
    app.run(debug=True, host=config.get("website", 'host'), port=int(config.get("website", 'port')))