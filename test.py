#!/usr/bin/env python3
# encoding: utf-8

"""
@author: WillSo
@license: Apache Licence 
@software: PyCharm
@file: test.py
@time: 2017\11\17 0017 14:37
"""

import time, datetime, LoggerFactory

def func():
    pass


class Main():
    def __init__(self):
        pass


class solution(object) :
    num = 0
    def add(self, i):
        self.num += i
        if (i == 0):
            return self.num
        else:
            return self.add(i - 1)

def add(i):
    if (i == 0) :
        return i
    else :
        return i + add(i-1)

if __name__ == '__main__':
    logger = LoggerFactory.Logger('D:/pythonLog/log/info.log', 'D:/pythonLog/log/error.log')
    today = datetime.datetime.today()
    logger.info(time.strptime('2018-04-04 16:30:30', '%Y-%m-%d %H:%M:%S') >=
          time.strptime(str(datetime.datetime(today.year, today.month, today.day, 16, 30, 0)), '%Y-%m-%d %H:%M:%S'))

    logger.info(time.strptime(str(datetime.datetime(today.year, today.month, today.day, 16, 30, 0)), '%Y-%m-%d %H:%M:%S'))