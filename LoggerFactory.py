#!/usr/bin/env python
# encoding: utf-8

"""
@author: WillSo
@license: Apache Licence 
@software: PyCharm
@file: loggerTest.py
@time: 2018\11\30 0030 16:09
"""

import logging, logging.handlers, datetime

class Logger :
    def __init__(self, infoFileName, errorFileName=None):
        self.logger = logging.getLogger('willso-logger')
        # 文件日志（info与error分开放）
        self.logger.setLevel(logging.DEBUG)
        self.rf_handler = logging.handlers.TimedRotatingFileHandler(infoFileName, when='midnight', interval=1,
                                                                    backupCount=7, atTime=datetime.time(0, 0, 0, 0))
        self.rf_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
        self.logger.addHandler(self.rf_handler)
        if errorFileName is not None and errorFileName != '' :
            self.f_handler = logging.FileHandler(errorFileName)
            self.f_handler.setLevel(logging.ERROR)
            self.f_handler.setFormatter(
                logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))
            self.logger.addHandler(self.f_handler)
    def debug(self, message):
        self.logger.debug(message)
    def info(self, message):
        self.logger.info(message)
    def warning(self, message):
        self.logger.warn(message)
    def error(self, message):
        self.logger.error(message,exc_info=True, stack_info=True)
    def critical(self, message):
        self.logger.critical(message)

if __name__ == '__main__':
    """
    logger = Logger('D:/pythonLog/log/info.log', 'D:/pythonLog/log/error.log')
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warning message')
    try :
        i = 1/0
    except :
        logger.error('error message')
    logger.critical('critical message')
    """
    pass